/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.suraida.midterm;

/**
 *
 * @author sulaiman
 */
public class Username {
    String id;
    String pass;
    
    
    Username(String id,String pass){
        this.id = id;
        this.pass = pass;
    }
    
    //กรอก id และรหัสอีกครั้งเพื่อยืนยันว่าการสมัครเสร็จ
    void charSuc(String id,String pass){
        System.out.println("Your Username is success!");
    }
    
    // เขียนชื่อตัวละครที่เราต้องการใช้
    void charName(String charn){
        System.out.println("Your character Name: " + charn);
    }
    
    //เลือกเพศตัวละครที่ต้องการเล่น
    void charGender(char charg){
        if(charg == 'M'){
            System.out.println("Your character Gender: Male");  
        }
        else if (charg == 'F'){
            System.out.println("Your character Gender: Female");  
        }
    }
    
        //เลือกอาชีพหรือประเภทตัวละครที่อยากใช้
        void charType(String chart){
            System.out.println("Your character Type: " + chart);
    }
        
        //เลือกธาตุที่อยากให้ตัวละครใช้
        void charElements(String chare){
            System.out.println("Your character Elements: " + chare);
        }
        
        //เลือกอาวุธที่ต้องการให้ตัวละครใช้
        void charWeapon(String charw){
            System.err.println("Your character Weapon: " + charw);
        }
    
}
