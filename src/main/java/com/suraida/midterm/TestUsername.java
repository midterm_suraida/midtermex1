/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.suraida.midterm;

/**
 *
 * @author sulaiman
 */
public class TestUsername {
    public static void main(String[] args) {
        Username user1 = new Username("lnwza007","12345678");
        
         // กรอก Username อีกครั้ง เหมือนกรณีเรากรอกไอดีเข้าเกม
        user1.charSuc("lnwza007","12345678");
        //ชื่อที่ตั้งให้ตัวละครในเกมชือ่ EXE
        user1.charName("EXE");
        //ตัวละครนี้กำหนดให้เป็นเพศชาย
        user1.charGender('M');
        //ตัวละครนี้จะเล่นสายอาชีพนักผจญภัย
        user1.charType("Adventurer");
        //ธาตุที่ตัวละครต้องการใช้คือ ธาตุไฟฟ้า
        user1.charElements("Electro");
        //อาวุธที่ตัวละครต้องการใช้คือ ดาบ
        user1.charWeapon("Sword");
    }
}
